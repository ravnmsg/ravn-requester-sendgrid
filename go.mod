module gitlab.com/ravnmsg/ravn-requester-sendgrid

go 1.16

require (
	github.com/sendgrid/rest v2.6.3+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.8.0+incompatible
	github.com/stretchr/testify v1.7.0 // indirect
	gitlab.com/ravnmsg/ravn-requester v0.0.0-20200918153239-4aeb3166afa3
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4 // indirect
)
