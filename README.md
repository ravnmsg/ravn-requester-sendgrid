# Ravn Sendgrid Requester

The Sendgrid Requester service delivers a message by email using [Sendgrid](https://sendgrid.com/).

## Configuration

A Sendgrid API Key must be specified using the `RAVN_REQUESTER_SENDGRID_API_KEY` environment variable.

By default, the service will listen to a random open port, but this can be configured with the `RAVN_REQUESTER_SENDGRID_SERVER_PORT` environment variable.

The service's latest Docker image can be pulled from `registry.gitlab.com/ravnmsg/ravn-requester-sendgrid:latest`. The following command will retrieve and start the service on port `50101`:

```bash
docker run --publish 50101:50101 --env RAVN_REQUESTER_SENDGRID_SERVER_PORT=50101 --env RAVN_REQUESTER_SENDGRID_API_KEY=(apikey) --rm registry.gitlab.com/ravnmsg/ravn-requester-sendgrid:latest
```

The service will then listen for POST requests on the `/request` path.

## Usage

```bash
$ curl -XPOST localhost:50101/request --data '{"from":"from@example.com","to":"to@example.com","subject":"Email Subject","content":"Email content."}' | jq
{
    "externalId": "qkuvR4gcB-iNW4cT1z7hJD"
}
```

## Adding to Ravn

This service will need to be configured as a delivery kind in Ravn. The following resources are required:

* A delivery kind with `host` set to the Sendgrid Requester service's host, eg. `localhost:50101`
* Two template kinds, one with `target_attr` set to `subject`, the other to `content`
* Two templates for each combination of message kind and language tag for which a Sendgrid email should be deliverable

The template body for `subject` will be generated as the email's subject. It can be static (eg. `Password Reset Link`) or dynamic (eg. `Password Result for Account {.accountName}`).

The template body for `content` will be generated as the email's body.

Template bodies are generated using [Go templates](https://golang.org/pkg/text/template/). All input provided in the initial request will be available in the template body.

Ravn can then receive requests to deliver messages using Sendgrid:

```bash
$ curl -XPOST https://api.ravnmsg.io/v1/namespaces/default/messages --data '{"messageKind":"password_reset","languageTag":"en","deliveryKind":"sendgrid","input":{"username":"John"}}' | jq
{
  "data": {
    "id": "62060571-bb5d-4c26-888f-79554c322b27",
    "messageKind": "password_reset",
    "languageTag": "en",
    "input": {
      "username": "John"
    },
    "status": "pending",
    "createTime": "2020-08-28T20:20:25.777151Z",
    "deliveries": []
  },
  "meta": {
    "requestId": "a2fa8d0a-4979-4e6e-a722-20b5f37a986d",
    "userRoleId": "17f5278e-6ef9-484a-b17e-c4b0b26e54c2"
  }
}
```
