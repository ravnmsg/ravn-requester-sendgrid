package sendgridrequester

import (
	"fmt"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"

	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
)

// Requester defines a Requester service whose execution results in a request to Sendgrid.
type Requester struct {
	client *sendgrid.Client
}

// New creates a new sendgrid Requester service.
func New(apiKey string) (*Requester, error) {
	if apiKey == "" {
		return nil, fmt.Errorf("error using Sendgrid API key: empty value")
	}

	r := &Requester{
		client: sendgrid.NewSendClient(apiKey),
	}

	return r, nil
}

// RequestFn performs a request to the Sendgrid third-party service.
//
// The following input values are required:
// * from, the source email address
// * to, the target email address
// * subject, the email's subject
// * content, the email's content
//
// If successful, the response will contain an X-Message-Id header that represents the external id,
// which will be added to the output.
func (r *Requester) RequestFn(input *requester.Values) (*requester.Values, error) {
	message, err := r.buildMessage(input)
	if err != nil {
		return nil, fmt.Errorf("error building request: %s", err)
	}

	response, err := r.client.Send(message)
	if err != nil {
		return nil, err
	} else if response.StatusCode != 202 {
		return nil, fmt.Errorf("error during request to Sendgrid: %s", response.Body)
	}

	var externalID string
	externalIDs, ok := response.Headers["X-Message-Id"]
	if ok && len(externalIDs) > 0 {
		externalID = externalIDs[0]
	}

	output := &requester.Values{
		"externalId": externalID,
	}

	return output, nil
}

func (r *Requester) buildMessage(input *requester.Values) (*mail.SGMailV3, error) {
	from, err := mail.ParseEmail(input.GetString("from"))
	if err != nil {
		return nil, err
	}

	to, err := mail.ParseEmail(input.GetString("to"))
	if err != nil {
		return nil, err
	}

	subject := input.GetString("subject")
	if subject == "" {
		return nil, fmt.Errorf("invalid subject with value `%v`", input.Get("subject"))
	}

	content := mail.NewContent("text/html", input.GetString("content"))

	return mail.NewV3MailInit(from, subject, to, content), nil
}
