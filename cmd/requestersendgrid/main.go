package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/ravnmsg/ravn-requester/pkg/app"
	"gitlab.com/ravnmsg/ravn-requester/pkg/server/httpserver"

	"gitlab.com/ravnmsg/ravn-requester-sendgrid/internal/requester/sendgridrequester"
)

func main() {
	srv, err := httpserver.New(os.Getenv("RAVN_REQUESTER_SENDGRID_SERVER_PORT"))
	if err != nil {
		log.Printf("Error creating HTTP server: %s", err)
		os.Exit(1)
	}

	requester, err := newSendgridRequester()
	if err != nil {
		log.Printf("Error configuring requester: %s", err)
		os.Exit(1)
	}

	if err := app.Start(context.Background(), srv, requester.RequestFn); err != nil {
		log.Printf("Error starting requester: %s", err)
		os.Exit(1)
	}
}

func newSendgridRequester() (*sendgridrequester.Requester, error) {
	apiKey := os.Getenv("RAVN_REQUESTER_SENDGRID_API_KEY")
	if apiKey == "" {
		return nil, fmt.Errorf("RAVN_REQUESTER_SENDGRID_API_KEY contains invalid API key with value `%s`", apiKey)
	}

	r, err := sendgridrequester.New(apiKey)
	if err != nil {
		return nil, err
	}

	return r, nil
}
